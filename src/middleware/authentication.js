const jwt = require('jsonwebtoken');
const User = require('../entities/user');

const {
  JWT_SECRET
} = process.env;


const authentication = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    // TODO: Create a Public Key. @Gareth
    const decoded = jwt.verify(token, JWT_SECRET);
    // eslint-disable-next-line no-underscore-dangle
    const user = await User.findOne({ _id: decoded._id, 'tokens.token': token });
    if (!user) {
      throw new Error();
    }

    req.token = token;
    req.user = user;
    next();
  } catch (error) {
    res.status(401).send({ error: 'Please authenticate' });
  }
};

module.exports = authentication;
