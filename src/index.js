const express = require('express');
require('./database/mongoose');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors');

const userRouter = require('./routers/users');
const swaggerDocument = require('../swagger.json');

const {
  PORT: port,
} = process.env;

const app = express();

app.use(cors());
// Default request decoding
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(express.json());

// Routes
app.use(userRouter);

// Start up server and listen
app.listen(port, () => {
  console.info('Server is up and running on', port);
});
