# ABSENT FUR BABIES API

## Developer Setup

### Requirements
- NodeJs: https://nodejs.org/en/download/
- Docker: https://hub.docker.com/editions/community/docker-ce-desktop-windows/
- MongoCompass: https://www.mongodb.com/download-center/compass
- Jetbrains Webstorm or any other sub par IDE.
- Heroku cli https://devcenter.heroku.com/articles/heroku-cli

### Database Setup
 After installing docker and Compass successfully. Run:
```sh
$ docker pull mongo
```
then
```sh
$ docker run -d -p 27017:27017 --name=mongodb  mongo
```
Open Compass and create a new connection naming. If you want to connect to the DEV DB use the connection string.

### Application Startup
```sh
npm install
```
#### Developement
```sh
$ npm run local
```

#### Production
```sh
 $ npm run start
```

Once the application has started use Compass to look through the database.

### Swagger Documentation
For all inquires revolving around API calls please look at the swagger documentation.
#### Local
http://localhost:3000/api-docs
#### Prod
https://absent-fur-babies-api.herokuapp.com/api-docs